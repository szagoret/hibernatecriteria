package com.run;

import com.entity.CustomersEntity;
import com.entity.EmployeesEntity;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.*;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.type.StandardBasicTypes;

import java.util.List;

/**
 * Created by szagoret on 5/13/2016.
 */
public class MainClass {
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    public static void main(String[] args) throws Throwable {
        sessionFactory = createSessionFactory();

        Criteria criteria = sessionFactory.openSession().createCriteria(CustomersEntity.class);
        //country equals France
        criteria.add(Restrictions.eq("country", "France"));

        //customerName less than 400
        criteria.add(Restrictions.lt("customerNumber", 400));

        //contactFirstName contains 'i'
        criteria.add(Restrictions.like("contactFirstName", "%i%"));

        // state is null
        criteria.add(Restrictions.isNull("state"));

        // criterion for or condition
        Criterion creditLimit = Restrictions.gt("creditLimit", 1000D);
        Criterion addressNotNull = Restrictions.isNotNull("addressLine2");

        // matching with or condition
        criteria.add(Restrictions.or(creditLimit, addressNotNull));

        // order
        criteria.addOrder(Order.desc("customerNumber"));
        // pagination
        criteria.setFirstResult(0);
        criteria.setMaxResults(5);

        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.property("customerNumber"), "customer");
        projectionList.add(Projections.property("customerName"), "customerName");
        projectionList.add(Projections.sqlProjection("city as result",
                new String[]{"result"}, new org.hibernate.type.Type[]{StandardBasicTypes.STRING}));
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Criteria.PROJECTION);
        List list = criteria.list();

        list.stream().forEach((customer) -> System.out.println(customer));
        System.out.println("Filtered size: " + list.size());

        criteria.setProjection(Projections.min("customerNumber"));
        Integer minValue = (Integer) criteria.uniqueResult();

        System.out.println("Min customer number: " + minValue);

        Criteria criteriaEmployee = sessionFactory.openSession().createCriteria(EmployeesEntity.class);
        criteriaEmployee.list();
    }

    public static SessionFactory createSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("/hibernate.cfg.xml");
        serviceRegistry = new ServiceRegistryBuilder().applySettings(
                configuration.getProperties()).buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }
}
